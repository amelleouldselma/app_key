// Components/Search.js
import React from 'react'
import {StyleSheet, Button, TextInput, View, Text, FlatList, Image } from 'react-native'

import {films} from '../Helpers/filmsData'

import FilmItem from './FilmItem'

class Search extends React.Component {
    // constructor(props: Props) {
    //     super(props)
    //     console.log('constructor')
    // }

    render(){
        return(
            <View style={styles.View} style={{ flex: 1, backgroundColor: '#2b2a2a' }}>
                <Image source={ require('../assets/logoKeyoss.png')}/>
                {/* <TextInput placeholder='Titre'/>
                <Button title='Keyoss' onPress={() => {}}/> */}

                <Text style={styles.Text}>Ajouter une video:</Text>
                <Button title='Ajouter un video' onPress={() => {}}/>

                <Text style={styles.Text}>Toute les videos:</Text>

                <FlatList
                    data={films}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <FilmItem film={item}/>}
                />
            </View>
        )
    }
    componentDidMount() {
        console.log('componentDidMount')
    }

  
}




const styles = StyleSheet.create( {
    View: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    borderColor: '#000000',
    borderWidth: 1,
    paddingLeft: 5,
    marginTop: 20, 
  },
  Text:{
    color: 'white'
  }

})

export default Search
